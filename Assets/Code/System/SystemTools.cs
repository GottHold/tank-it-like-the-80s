﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This class offers tools for the runtime level
/// </summary>
public class SystemTools : MonoBehaviour {

    /// <summary>
    /// Quits the Application
    /// </summary>
    public void exit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Changes the Level To a another Scene
    /// </summary>
    /// <param name="name"></param>
    public void changeScene(string name)
    {
        Debug.Log("Loading new Scene: "+name);
        SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);
    }
}
