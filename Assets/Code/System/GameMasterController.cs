﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is highest level controller of the game, controlling the state of the game
/// </summary>

public class GameMasterController :MonoBehaviour  {

  
    private static int playerIdOffset = 0;
    private static int teamIdOffset = 0;
    public static LevelController levelController;
    public static UIController uiController;
    public static SystemTools sysTools;

    private static GameState _gameState = GameState.Undefined;
    public static GameState gameSate
    {
        get
        {
            return _gameState;
        }
        set
        {
            _gameState = value;
        }
    }


	
}

public enum GameState { Undefined, MainMenu, Paused, Loading, Ingame,Preparing, Pregame, Postgame};