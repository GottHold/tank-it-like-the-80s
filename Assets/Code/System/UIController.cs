﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

    public GameObject m_scoreContentUI;
    public GameObject m_timeContentUI;
    public GameObject m_levelContentUI;
    public GameObject m_playerContentUI;
    public GameObject m_baseContentUI;
    public GameObject m_enemiesContentUI;

    private void Start()
    {
        GameMasterController.uiController = this;
    }
}
