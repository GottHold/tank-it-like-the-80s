﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
///  This Class offers Tools for UI Handling.
/// </summary>
public class UITools : MonoBehaviour {


    /// <summary>
    /// Sets the color of a Text element that is sitting on the GameObject given by the Parameter
    /// </summary>
    /// <param name="color">Color that should be set.</param>
    /// <param name="gameOb">GameObject on which the Text element sits.</param>
	public void setColorOfTextComponent(Color color, GameObject gameOb)
    {
        Text text = gameOb.GetComponent<Text>();
        if (text != null)
        {
            text.color = color;
        }
        else
        {
            Debug.LogError(gameOb.transform.name + " does not contain Text component. (UITools::setColorOfTextComponent)");
        }
    }

    /// <summary>
    /// returns Color of the Text element that is on the GameObject. If there is none it returns violet. (1,0,1)
    /// </summary>
    /// <param name="gameOb"> Game Object with the Text Element</param>
    /// <returns>returns the color of the Text. If its an error it returns violet.</returns>
    public Color getColorOfTextComponent(GameObject gameOb)
    {
        Text text = gameOb.GetComponent<Text>();
        if (text != null)
        {
            return text.color;
        }
        else
        {
            Debug.LogError(gameOb.transform.name + " does not contain Text component. (UITools::getColorOfTextComponent)");
        }
        return new Color(1.0f,0.0f,1.0f);
    }

    /// <summary>
    /// Sets UI Text to a specific string and takes care of the exception Handling
    /// </summary>
    /// <param name="gameOb">GameObject on which the text element Lays</param>
    /// <param name="content">String content that should be transferred to the Text Object</param>
    public void setTextofTextComponentTo(GameObject gameOb, string content)
    {
        Text text = gameOb.GetComponent<Text>();
        if (text != null)
        {
            text.text = content;
        }
        else
        {
            Debug.LogError(gameOb.transform.name + " does not contain Text component. (UITools::setTextofTextComponentTo)");
        }
    }
}
