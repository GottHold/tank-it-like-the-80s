﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class controls the main menu and all its items
/// </summary>
public class MainMenuControl : MonoBehaviour {
    public List<GameObject> m_MainMenuItems;
    public int m_defaultSelectedItem;
    private Color _m_highlightedItem;
    private UITools m_uiTools;
    public Color m_highlightedItem
    {
        //Directly apply logic to all needed Entities.
        get
        {
            return _m_highlightedItem;
        }
        set
        {
            changeColorOfAMenuItem(m_MenuSelected, value);
            _m_highlightedItem = value;
        }
    }
    public Color m_unhighlightedItem
    {
        //Directly apply logic to all needed Entities.
        get
        {
            return _m_unhighlightedItem;
        }
        set
        {
            changeColorOfAllMenuItems(value);
            _m_unhighlightedItem = value;
            changeColorOfAMenuItem(m_MenuSelected,m_highlightedItem);
        }
    }
    private Color _m_unhighlightedItem;
    private int _m_MenuSelected;
    private int m_MenuSelected
    {
        //Directly apply logic to all needed Entities.
        set
        {
            //Recursion if flip is needed in either direction.
            if (value < 0)
            {
                value = m_MainMenuItems.Count - value;
                m_MenuSelected = value;
                return;
            }else if (value >= m_MainMenuItems.Count)
            {
                value = value - m_MainMenuItems.Count;
                m_MenuSelected = value;
                return;
            }
            changeColorOfAMenuItem(_m_MenuSelected, m_unhighlightedItem);
            _m_MenuSelected = value;
            changeColorOfAMenuItem(_m_MenuSelected, m_highlightedItem);
        }
        get
        {
            return _m_MenuSelected;
        }
    }

	
	void Start () {
        // Use this for initialization
        GameMasterController.gameSate = GameState.MainMenu;
        m_uiTools = gameObject.AddComponent<UITools>();
        if (m_defaultSelectedItem < 0 || m_defaultSelectedItem >= m_MainMenuItems.Count) m_defaultSelectedItem = 0;
        m_highlightedItem = m_uiTools.getColorOfTextComponent(m_MainMenuItems[m_defaultSelectedItem]);
        if (0 == m_defaultSelectedItem)
        {
            if (m_MainMenuItems.Count >= 2)
            {
                m_unhighlightedItem = m_uiTools.getColorOfTextComponent(m_MainMenuItems[1]);
            }
            else
            {
                Debug.LogWarning("Not enough items to set dehighlighted Menu Item color");
                m_unhighlightedItem = new Color();
            }
        }
        else
        {
            m_unhighlightedItem = m_uiTools.getColorOfTextComponent(m_MainMenuItems[0]);
        }
        
	}
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetKeyDown(KeyCode.LeftAlt)|| Input.GetKeyDown(KeyCode.RightAlt))&& Input.GetKeyDown(KeyCode.F4))
        {
            SystemTools systools = gameObject.AddComponent<SystemTools>();
            systools.exit();
        }
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            m_MenuSelected -= 1;
        }
        if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            m_MenuSelected += 1;
        }
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
        {
            enterSelection();
        }
    }

    /// <summary>
    /// This function will change all Menu items to a certain color
    /// 
    /// </summary>
    /// <param name="color">Color which will be applied to the Menu items</param>
    void changeColorOfAllMenuItems(Color color)
    {
        for (int i = 0; i < m_MainMenuItems.Count; ++i)
        {
            changeColorOfAMenuItem(i, color);
        }
    }

    /// <summary>
    /// This function will change the color of a Menu Item.
    /// </summary>
    /// <param name="item">Index of the Menu item in the m_MainMenuItems list</param>
    /// <param name="color">Color to which it should change</param>
    void changeColorOfAMenuItem(int item, Color color)
    {   
        if (item < 0 || item >= m_MainMenuItems.Count)
        {
            Debug.LogError("MainMenuControl::changeColorOfMenuItem, item out of bounce (below 0 or max size)");
        }
        m_uiTools.setColorOfTextComponent(color, m_MainMenuItems[item]);
    }

    /// <summary>
    /// Enters Selection of Menu
    /// </summary>
    void enterSelection()
    {
        PointerScript pointer = m_MainMenuItems[m_MenuSelected].GetComponent<PointerScript>();
        if (pointer != null)
        {
            pointer.pointingTo.Invoke();
        }
        else
        {
            Debug.LogError(m_MainMenuItems[m_MenuSelected].transform.name + " does not have a pointer script");
        } 
    }
}
