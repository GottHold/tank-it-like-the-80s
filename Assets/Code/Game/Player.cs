﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : PlayerEntity {

    
    public float m_movementCooldown;
    public float m_fireCooldown;
    public float m_restTimeFromCoolDownMovement = 0.0f;
    public float m_restTimeFromCoolDownFire = 0.0f;
    private SystemTools m_sysTools;
    

    private void Start()
    {
        GameMasterController.sysTools = new SystemTools();
        m_movementCooldown = 0.25f;
        m_fireCooldown = 0.5f;
        
    }

    public virtual void Update()
    {
        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.F4)) m_sysTools.exit();
        if(GameMasterController.gameSate== GameState.Ingame)
        {
            if (m_restTimeFromCoolDownMovement < 0.0f)
            {
                if(Input.GetKey(KeyCode.UpArrow)|| Input.GetKey(KeyCode.W))
                {
                    directionalInteract(direction.up);
                    m_restTimeFromCoolDownMovement = m_movementCooldown;
                } else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                {
                    directionalInteract(direction.down);
                    m_restTimeFromCoolDownMovement = m_movementCooldown;
                } else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                {
                    directionalInteract(direction.left);
                    m_restTimeFromCoolDownMovement = m_movementCooldown;
                } else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
                {
                    directionalInteract(direction.right);
                    m_restTimeFromCoolDownMovement = m_movementCooldown;
                }
                
            }
            else
            {

                m_restTimeFromCoolDownMovement -= Time.deltaTime;
            }
            if (m_restTimeFromCoolDownFire < 0.0f)
            {
                if (Input.GetKey(KeyCode.Space))
                {
                    fire("ProjectilePlayer");
                    m_restTimeFromCoolDownFire = m_fireCooldown;
                    m_restTimeFromCoolDownMovement = m_movementCooldown;
                }
            }
            else
            {
                m_restTimeFromCoolDownFire -= Time.deltaTime;
            }
        }
    }

    /// <summary>
    /// You need to first look into the direction and then you can drive towards it
    /// </summary>
    /// <param name="dir"></param>
    protected void directionalInteract(direction dir)
    {
        if (dir == m_direction)
        {
            switch (dir)
            {
                case direction.up:
                    if (Convert.ToInt32(m_position.y) + 1 < m_currentLevel.m_size)
                        moveTo(m_position + new Vector2(0, 1));
                    break;
                case direction.down:
                    if (Convert.ToInt32(m_position.y) - 1 >= 0)
                        moveTo(m_position - new Vector2(0, 1));
                    break;
                case direction.left:
                    if (Convert.ToInt32(m_position.x) - 1 >= 0)
                        moveTo(m_position - new Vector2(1, 0));
                    break;
                case direction.right:
                    if (Convert.ToInt32(m_position.x) + 1 < m_currentLevel.m_size)
                        moveTo(m_position + new Vector2(1, 0));
                    break;
                default:break;
            }
        }
        else
        {
            if (getDirectionalDifference(dir) == directionalDifference.left)
            {
                rotate(1, rotationDirection.counterClockWise);
            }
            else
            {
                rotate(1, rotationDirection.clockWise);
            }
        }
    }

    /// <summary>
    /// Direction from which the dir is offset in regards to the player direction
    /// </summary>
    /// <param name="dir"></param>
    /// <returns>Return the direction the entity needs to turn to match player input</returns>
    directionalDifference getDirectionalDifference(direction dir)
    {
        if (m_direction == dir) return directionalDifference.none;
        if ((m_direction == direction.up && dir == direction.left) ||
            (m_direction == direction.left && dir == direction.down) ||
            (m_direction == direction.down && dir == direction.right) ||
            (m_direction == direction.right && dir == direction.up)
            ) return directionalDifference.left;
        else return directionalDifference.right;
    }

    /// <summary>
    /// When a player wants to fire a projectile
    /// </summary>
    /// <param name="name">Name of the Projectile Prefab</param>
    protected void fire(string name)
    {
        Vector2 vectorDirection;
        float zAngle = 0.0f;
        switch (m_direction)
        {
            case direction.up:
                vectorDirection =  new Vector2(0, 1);
                zAngle = 180.0f;
                break;
            case direction.down:
                vectorDirection =  new Vector2(0, -1);
                zAngle = 0.0f;
                break;
            case direction.left:
                vectorDirection =  new Vector2(-1, 0);
                zAngle = 270.0f;
                break;
            case direction.right:
                vectorDirection =  new Vector2(1, 0);
                zAngle = 90.0f;
                break;
            default: vectorDirection = new Vector2();break;
        }
        if (!(m_position.x+vectorDirection.x >= 0 && m_position.y + vectorDirection.y >= 0 && m_position.x +
            vectorDirection.x < m_currentLevel.m_size && m_position.y + vectorDirection.y < m_currentLevel.m_size)) return;
        GameObject projectileGameObj = Instantiate(GameMasterController.levelController.m_projectilesPrefab[name],
            m_currentLevel.gridVector2ToVector3(m_position+vectorDirection), Quaternion.identity);
        
        projectileGameObj.transform.eulerAngles = new Vector3(0, 0, zAngle);
        projectileGameObj.transform.localScale=projectileGameObj.transform.localScale* m_currentLevel.m_positionScaleFactor * 0.1f;
        Projectile projectile = projectileGameObj.GetComponentInChildren<Projectile>();
        projectile.m_direction = vectorDirection;

    }

    

}

public enum directionalDifference { left, right, none};
