﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsController : MonoBehaviour
{

    public GameMode m_gameMode;
    public Level m_level;
    private int m_points=0;
    UITools m_uiTools=new UITools();

    public void addPoints(int points)
    {
        m_points += points;
        m_uiTools.setTextofTextComponentTo(GameMasterController.uiController.m_scoreContentUI, m_points.ToString());
    }

    public void resetPoints()
    {
        m_points = 0;
        m_uiTools.setTextofTextComponentTo(GameMasterController.uiController.m_scoreContentUI, m_points.ToString());
    }

    public void minusPoints(int points)
    {
        m_points -= points;
        m_uiTools.setTextofTextComponentTo(GameMasterController.uiController.m_scoreContentUI, m_points.ToString());
    }

    // Update is called once per frame
    void Update()
    {

    }
}
