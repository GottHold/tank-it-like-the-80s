﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// This Class controls all Resources around levels. It can create level. It can spawn game modes on it. And provide resources for Entities on a lower hierarchy for it.
/// </summary>
public class LevelController : MonoBehaviour {
    private Dictionary<string, Level> m_levelMap;
    public Dictionary<string, GameObject> m_gameModePrefab;
    public Dictionary<string, GameObject> m_assetsPrefab;
    public Dictionary<string, GameObject> m_playerEntitiesPrefab;
    public Dictionary<string, GameObject> m_projectilesPrefab;
    public List<GameObject> m_gameAssetsToIndex;
    public List<GameObject> m_gameModesToIndex;
    public List<GameObject> m_playerEntitiesToIndex;
    public List<GameObject> m_projectilesToIndex;
    public int m_defaultLevelSize;
    public int m_defaultGameMode;
    public int m_borderAssets;
    Dictionary<string, int> testLevel = new Dictionary<string, int>();
    private GameObject _m_currentGameModeGameObj;
    public int m_freeMapNameInt
    {
        get
        {
            int i = 0;
            while (m_levelMap.ContainsKey(i.ToString()))
            {
                i++;
            }
            return i;
        }
    }
    public GameObject m_currentGameModeGameObj
    {
        get
        {
            return _m_currentGameModeGameObj;
        }
        set
        {

        }
    }
    private string _m_currentLevel;
    private GameObject _m_currentLevelGameObj;
    public GameObject m_currentlevelGameObj
    {
        get
        {
            return _m_currentLevelGameObj;
        }
        set
        {
            _m_currentLevelGameObj = value;
        }
    }
    public float m_levelAreaOffset;
    public string m_borderAsset;
    public string m_currentLevel
    {
        get
        {
            return _m_currentLevel;
        }
        set
        {
            if (m_levelMap.ContainsKey(value))
            {
                if(_m_currentLevel.Length>0)
                m_levelMap[_m_currentLevel].close();
                _m_currentLevel = value;
                m_levelMap[value].display();
                _m_currentLevelGameObj = m_levelMap[value].gameObject;
            }
            else
            {
                Debug.LogError("Map does not exist in dictionary. Can't set Level to it");
            }
        }
    }

    /// <summary>
    /// Initialization of the object
    /// </summary>
    private void Start()
    {
        GameMasterController.levelController = this;
        GameMasterController.gameSate = GameState.Preparing;
        _m_currentLevel = "";
        m_assetsPrefab = new Dictionary<string, GameObject>();
        m_levelMap = new Dictionary<string, Level>();
        m_gameModePrefab = new Dictionary<string, GameObject>();
        m_playerEntitiesPrefab = new Dictionary<string, GameObject>();
        m_projectilesPrefab = new Dictionary<string, GameObject>();
        indexGameObjects(m_gameAssetsToIndex, m_assetsPrefab);
        indexGameObjects(m_gameModesToIndex, m_gameModePrefab);
        indexGameObjects(m_playerEntitiesToIndex, m_playerEntitiesPrefab);
        indexGameObjects(m_projectilesToIndex, m_projectilesPrefab);
        testLevel.Add("DestructableWallInv", m_borderAssets);
        generateLevel("test", m_defaultLevelSize, testLevel);
        m_currentLevel = "test";
        instentiateGameMode("GameMode_Survive");
        
        
    }

    /// <summary>
    /// Indexes Resources from the Editor into the code
    /// </summary>
    /// <param name="source">The Ressource List which is there to index</param>
    /// <param name="destination">The destination which is going to be filled</param>
    void indexGameObjects(List<GameObject> source, Dictionary<string, GameObject> destination)
    {
        destination.Clear();
        foreach(GameObject gameObj in source)
        {
            string name = gameObj.transform.name;
            if (!destination.ContainsKey(name))
            {
                //Debug.Log(name);
                destination.Add(name, gameObj);
            }
            else
            {
                Debug.LogError("Duplicated Index names in indexer. " + name);
            }
        }
    }

    public void generateLevel(string levelName)
    {
        generateLevel(levelName, m_defaultLevelSize, testLevel);
    }


    /// <summary>
    ///  Generates Randomized Game Level
    /// </summary>
    /// <param name="levelName">the name under which the paper design of the level will be found in the dictionary</param>
    /// <param name="size">the size in blocks of the map</param>
    /// <param name="countOfEachAsset">Dictionary List of each item that should be spawned randomly on the map</param>
    public void generateLevel(string levelName, int size, Dictionary<string,int> countOfEachAsset)
    {
        if (m_levelMap.ContainsKey(levelName))
        {
            Debug.LogError("Level with that name already exists");
            return;
        }
        int maxLevelSize;
        if (size <= 0) maxLevelSize =  m_defaultLevelSize;
        else maxLevelSize =  size;
        Dictionary<Vector2, string> gameMap = new Dictionary<Vector2, string>();
        foreach(KeyValuePair<string, int> assetInfoPair in countOfEachAsset)
        {
            if (!m_assetsPrefab.ContainsKey(assetInfoPair.Key)) continue;
            for(int i=0; i<assetInfoPair.Value; ++i)
            {
                Vector2 assetLocation= new Vector2();
                bool isFree = false;
                while (!isFree)
                {
                    assetLocation = new Vector2(Mathf.Floor(UnityEngine.Random.Range(0.0f, Convert.ToSingle(maxLevelSize))), Mathf.Floor( UnityEngine.Random.Range(0.0f, Convert.ToSingle(maxLevelSize))));
                    if (!gameMap.ContainsKey(assetLocation)) isFree = true;
                }
                gameMap.Add(assetLocation, assetInfoPair.Key);
            }
        }

        if (gameMap.Count == 0)
        {
            Debug.LogError("Level couldn't be created. Something went wrong");
            return;
        }

        GameObject levelGameObject = new GameObject("Level_" + levelName);
        Level level = levelGameObject.AddComponent<Level>();
        level.m_positionScaleFactor = 10 / Convert.ToSingle(size + 2) *((100-m_levelAreaOffset)/100) ;
        level.m_paperDesignAssets = gameMap;
        level.m_levelControler = this;
        level.m_size = size;
        level.m_assetBorder = m_borderAsset;
        m_levelMap.Add(levelName, level);

    }

    /// <summary>
    /// Spawns the game mode controller
    /// </summary>
    /// <param name="gameModeName">Name of the game mode</param>
    public void instentiateGameMode(string gameModeName)
    {
        if (!m_gameModePrefab.ContainsKey(gameModeName))
        {
            Debug.LogError("Game Mode with that name was not indexed");
            return;
        }
        _m_currentGameModeGameObj = Instantiate(m_gameModePrefab[gameModeName]);
        GameMode gameMode = _m_currentGameModeGameObj.GetComponent<GameMode>();
        gameMode.m_levelController = this;
        gameMode.prepare();
    }


    public void loadLevelFromFileToDict(string levelName, string fileLocation, FileInputType inputType)
    {

    }

    public void saveLevelFromDictToFile(string levelName, string fileLocation, FileInputType inputType)
    {

    }


}

public enum FileInputType { Text, Binary};
