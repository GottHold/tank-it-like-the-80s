﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This Class defines the basic logic and attributes every Game Mode has
/// </summary>
public class GameMode : MonoBehaviour
{
    public LevelController m_levelController;
    public string m_gameModeName="Undefined";
    public float m_gameModeMaxTime = 1800;
    public UnityEvent m_gameStarted= new UnityEvent();
    public UnityEvent m_gameEnded = new UnityEvent();
    public float m_gameTime = 0.0f;
    public float m_preRoundTimer = 10.0f;
    public Dictionary<string, Team> m_teams;
    protected Level m_currentLevel;
    protected UITools m_uiTools;
    public int m_score = 0;
    public StatsController m_statsController;


    /// <summary>
    /// State Machine for the overall game state during the game mode period
    /// </summary>
    protected void stateMachineHandler()
    {
        switch (GameMasterController.gameSate)
        {
            case GameState.Pregame:
                m_preRoundTimer -= Time.deltaTime;
                m_uiTools.setTextofTextComponentTo(GameMasterController.uiController.m_timeContentUI, (m_preRoundTimer * -1.0f).ToString());
                if (m_preRoundTimer <= 0)
                {
                    GameMasterController.gameSate = GameState.Ingame;
                    m_gameStarted.Invoke();
                    m_preRoundTimer = 10.0f;
                }
                break;
            case GameState.Ingame:
                m_gameTime += Time.deltaTime;
                m_uiTools.setTextofTextComponentTo(GameMasterController.uiController.m_timeContentUI, m_gameTime.ToString());
                if (m_gameTime >= m_gameModeMaxTime)
                {
                    end();
                }
                break;
            default: break;
        }
    }


    /// <summary>
    /// Starts the Pre-round timer and then the game mode
    /// </summary>
    public virtual void start()
    {
        GameMasterController.gameSate = GameState.Pregame;
        m_uiTools = new UITools();

    }

    /// <summary>
    /// Ends the game round
    /// </summary>
    public virtual void end()
    {
        GameMasterController.gameSate = GameState.Postgame;
        m_gameEnded.Invoke();
    }

    public virtual void prepare()
    {
        m_teams = new Dictionary<string, Team>();
    }

}
