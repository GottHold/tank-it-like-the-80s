﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines the Level with all important components
/// </summary>
public class Level : MonoBehaviour {

    public int m_size;
    public Dictionary<Vector2, string> m_paperDesignAssets;
    public Dictionary<Vector2, GameObject> m_assets;
    public LevelController m_levelControler;
    private float _m_positionScaleFactor;
    public float m_positionScaleFactor
    {
        get
        {
            return _m_positionScaleFactor;
        }
        set
        {
            _m_positionScaleFactor = value;
            m_offSetVector = -1 * new Vector3(5.0f, 5.0f)
                + new Vector3(m_positionScaleFactor, m_positionScaleFactor) / 2
                + new Vector3(m_positionScaleFactor * 2, m_positionScaleFactor);
        }
    }
    public const float SCALECONSTANT = 0.1f;
    public string m_assetBorder;
    private Vector3 m_offSetVector;

    /// <summary>
    /// Spawns the Level Assets from the Level paper design
    /// </summary>
    public void display()
    {
        m_assets = new Dictionary<Vector2, GameObject>();
        foreach (KeyValuePair<Vector2, string> asset in m_paperDesignAssets)
        {
            GameObject assetGameObject = Instantiate(m_levelControler.m_assetsPrefab[asset.Value],
                gridVector2ToVector3(asset.Key), Quaternion.identity);
            assignGameObjectToSceneAndParent(assetGameObject, asset.Key);
            Destructable destructable = assetGameObject.GetComponentInChildren<Destructable>();
            if (destructable != null)
            {
                destructable.m_position = asset.Key;
            }
        }
        GameObject lowerBorder = Instantiate(m_levelControler.m_assetsPrefab[m_assetBorder],
            gridVector2ToVector3(new Vector2(Convert.ToSingle(m_size) / 2, 0), new Vector2(0, -1)), Quaternion.identity);
        assignGameObjectToSceneAndParent(lowerBorder, new Vector2(Convert.ToSingle(m_size) / 2, -1));
        GameObject upperBorder = Instantiate(m_levelControler.m_assetsPrefab[m_assetBorder],
            gridVector2ToVector3(new Vector2(Convert.ToSingle(m_size) / 2, m_size)), Quaternion.identity);
        assignGameObjectToSceneAndParent(upperBorder, new Vector2(Convert.ToSingle(m_size) / 2, m_size));
        GameObject leftBorder = Instantiate(m_levelControler.m_assetsPrefab[m_assetBorder],
            gridVector2ToVector3(new Vector2(0, Convert.ToSingle(m_size) / 2), new Vector2(-1, 0)), Quaternion.identity);
        assignGameObjectToSceneAndParent(leftBorder, new Vector2(-1, Convert.ToSingle(m_size) / 2));
        GameObject rightBorder = Instantiate(m_levelControler.m_assetsPrefab[m_assetBorder],
            gridVector2ToVector3(new Vector2(m_size, Convert.ToSingle(m_size) / 2)), Quaternion.identity);
        assignGameObjectToSceneAndParent(rightBorder, new Vector2(m_size, Convert.ToSingle(m_size) / 2));
        leftBorder.transform.localScale = new Vector3(leftBorder.transform.localScale.x, 1, leftBorder.transform.localScale.z);
        leftBorder.transform.position += new Vector3(0.0f, (m_positionScaleFactor / 2)*-1, 0.0f);
        rightBorder.transform.localScale = new Vector3(rightBorder.transform.localScale.x, 1, rightBorder.transform.localScale.z);
        rightBorder.transform.position += new Vector3(0.0f, (m_positionScaleFactor / 2) * -1, 0.0f);
        upperBorder.transform.localScale = new Vector3(1, upperBorder.transform.localScale.y, lowerBorder.transform.localScale.z);
        lowerBorder.transform.localScale = new Vector3(1, lowerBorder.transform.localScale.y, lowerBorder.transform.localScale.z);
        lowerBorder.transform.position += new Vector3((m_positionScaleFactor / 2) * -1, 0.0f , 0.0f);
        upperBorder.transform.position += new Vector3((m_positionScaleFactor / 2) * -1, 0.0f, 0.0f);
        //m_levelControler.m_currentlevelGameObj = this.gameObject;


    }

    /// <summary>
    /// Assigns a GameObject to the Level with its 2d position
    /// </summary>
    /// <param name="gameObj">Gameobject with asset</param>
    /// <param name="scenePos">Position of Object</param>
    public void assignGameObjectToScene(GameObject gameObj, Vector2 scenePos)
    {
        gameObj.transform.localScale = gameObj.transform.localScale * m_positionScaleFactor * SCALECONSTANT;
        gameObj.transform.eulerAngles = new Vector3(0, 0, 0);
        m_assets.Add(scenePos, gameObj);
    }


    public void assignGameObjectToSceneAndParent(GameObject gameObj, Vector2 scenePos)
    {
        assignGameObjectToScene(gameObj, scenePos);
        gameObj.transform.SetParent(gameObject.transform);
    }

    /// <summary>
    /// Calculates the 2d Position to a 3d Scene Position
    /// </summary>
    /// <param name="vector">the original position which should be converted</param>
    /// <param name="offset">If the Original still needs an offset of some sort, that can be given here</param>
    /// <returns></returns>
    public Vector3 gridVector2ToVector3(Vector2 vector, Vector2 offset)
    {
        Vector3 result = new Vector3(vector.x, vector.y) * m_positionScaleFactor + m_offSetVector 
            + new Vector3(offset.x, offset.y,0.0f) * m_positionScaleFactor;
        return result;
    }

    /// <summary>
    /// Calculates the 2d Position to a 3d Scene Position
    /// </summary>
    /// <param name="vector">the original position which should be converted</param>
    /// <returns></returns>
    public Vector3 gridVector2ToVector3(Vector2 vector)
    {
        return gridVector2ToVector3(vector, new Vector2());
    }

    /// <summary>
    /// Spawns asset in Level
    /// </summary>
    /// <param name="position">2d position of the Asset</param>
    /// <param name="name">the name of the Asset in the library</param>
    public void spawnAsset(Vector2 position, string name)
    {
        GameObject gameObj = Instantiate(m_levelControler.m_assetsPrefab[name],
            gridVector2ToVector3(position), Quaternion.identity);
        assignGameObjectToSceneAndParent(gameObj, position);
    }

    /// <summary>
    /// Function returns a position on the grid that is unused.
    /// </summary>
    /// <returns>returns 2d Location</returns>
    public Vector2 randomPositionThatIsUnused()
    {
        Vector2 result = new Vector2();
        bool foundFreeSpot = false;
        while (!foundFreeSpot)
        {
            result= new Vector2(Mathf.Floor(UnityEngine.Random.Range(0.0f, Convert.ToSingle(m_size))), Mathf.Floor(UnityEngine.Random.Range(0.0f, Convert.ToSingle(m_size))));
            if (!m_assets.ContainsKey(result)) break;
        }
        return result;
    }

    /// <summary>
    /// Closes current Level and its Assets
    /// </summary>
    public void close()
    {
        foreach(KeyValuePair<Vector2, GameObject> assets in m_assets)
        {
            Destroy(assets.Value);
        }
        m_assets.Clear();
        
    }
}
