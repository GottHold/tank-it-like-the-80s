﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructable : MonoBehaviour {
    Level m_level;
    private float _m_health;
    public Vector2 m_position;
    public float m_health
    {
        get
        {
            return _m_health;
        }
        set
        {
            _m_health = value;
            if (value <= 0.0f)
            {
                clearMassets();
                Destroy(this.gameObject);
            }
                
                
        }
    }

	// Use this for initialization
	void Start () {
        _m_health = 1.0f;
        m_level = GameMasterController.levelController.m_currentlevelGameObj.GetComponent<Level>();
	}

    private void OnDestroy()
    {
        clearMassets();
    }

    private void OnDisable()
    {
        clearMassets();
    }

    void clearMassets()
    {
        if (m_level.m_assets.ContainsKey(m_position))
            m_level.m_assets.Remove(m_position);
    }

}
