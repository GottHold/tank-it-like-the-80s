﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class defines the Basic PlayerEntity Class and everything around it
/// </summary>
public class PlayerEntity : MonoBehaviour {

    public float m_startHealth = 1.0f;
    protected float _m_health=1.0f;
    public float m_health
    {
        get
        {
            return _m_health;
        }
        set
        {
            _m_health = value;
            
            if (_m_health <= 0.0f)
            {
                m_playerDeath.Invoke();
                m_currentLevel.m_assets.Remove(m_position);
                gameObject.SetActive(false);
            }      
            else
            {
                m_playerHealth.Invoke();
            }
            
        }
    }
    public UnityEvent m_playerDeath = new UnityEvent();
    public UnityEvent m_playerSpawmed = new UnityEvent();
    public UnityEvent m_playerAction = new UnityEvent();
    public UnityEvent m_playerHealth = new UnityEvent();
    public Vector2 m_position;
    public List<Team> m_partOfTeams;
    public Level m_currentLevel;
    public direction m_direction = direction.down;

    private void Start()
    {
        _m_health = m_startHealth;
    }

    /// <summary>
    /// Moves the player to the next Location
    /// </summary>
    /// <param name="position">The position the playerEntity should move to</param>
    public void moveTo(Vector2 position)
    {
        if (m_currentLevel.m_assets.ContainsKey(position)) return;
        if (m_currentLevel.m_assets.ContainsKey(m_position))
        {
            m_currentLevel.m_assets.Remove(m_position);
        }
        m_position = position;
        m_currentLevel.m_assets.Add(m_position, gameObject);
        this.gameObject.transform.position = m_currentLevel.gridVector2ToVector3(position);
    }

    /// <summary>
    ///  Rotates the Player entity in steps
    /// </summary>
    /// <param name="steps">amount of 90° steps that it should be turned</param>
    /// <param name="rdirection">direction in which it should be turned</param>
    public void rotate(int steps, rotationDirection rdirection)
    {
        if(rdirection== rotationDirection.clockWise)
        {
            this.gameObject.transform.eulerAngles -= new Vector3(0.0f, 0.0f, 90.0f*steps);
        }
        else
        {
            this.gameObject.transform.eulerAngles += new Vector3(0.0f, 0.0f, 90.0f * steps);
        }
        switch (Convert.ToInt32(this.gameObject.transform.eulerAngles.z))
        {
            case 0: m_direction = direction.down;break;
            case 90: m_direction = direction.right; break;
            case 180: m_direction = direction.up; break;
            case 270: m_direction = direction.left; break;
            default:break;
        }
    }

    public virtual void OnDestroy()
    {
        if (m_currentLevel.m_assets.ContainsKey(m_position))
            m_currentLevel.m_assets.Remove(m_position);
    }

}

public enum rotationDirection { clockWise, counterClockWise};
public enum direction { up, left, right, down};