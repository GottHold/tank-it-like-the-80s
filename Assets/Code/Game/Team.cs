﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Defines the Team Base class with the attributes every Team presumably needs.
/// </summary>
public class Team : MonoBehaviour {
    public string m_teamName;
    public string m_teamId;
    public Dictionary<string, PlayerEntity> m_players;
    public float m_combinedHP;
    public int m_playerAlive;
    public UnityEvent m_teamExterminated = new UnityEvent();
    public UnityEvent m_teamHealthChanged = new UnityEvent();

    public void connectEvents()
    {
        foreach (KeyValuePair<string, PlayerEntity> player in m_players)
        {
            player.Value.m_playerDeath.AddListener(playerDied);
            player.Value.m_playerHealth.AddListener(healthChanged);
        }
    }

    void recheckHealth()
    {
        m_combinedHP = 0;
        foreach (KeyValuePair<string, PlayerEntity> player in m_players)
        {
            m_combinedHP += player.Value.m_health;
        }
    }

    /// <summary>
    /// Reevaluates the aggregated health of the team
    /// </summary>
    void healthChanged()
    {
        recheckHealth();
        m_teamHealthChanged.Invoke();
    }

    void recheckDied()
    {
        m_playerAlive = 0;
        foreach (KeyValuePair<string, PlayerEntity> player in m_players)
        {
            if (player.Value.m_health > 0.0f)
            {
                ++m_playerAlive;
            }
        }
    }

    /// <summary>
    /// Reevaluates the amount of players Alive
    /// </summary>
    void playerDied()
    {
        recheckDied();
        if (m_playerAlive == 0)
        {
            m_teamExterminated.Invoke();
        }
    }

    public virtual void init()
    {
        recheckHealth();
        recheckDied();
    }

    
}
