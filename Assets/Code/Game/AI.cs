﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : Player
{
    private PlayerEntity m_target;
    private Dictionary<string, Team> m_allTeams;

    private void Start()
    {
        _m_health = m_startHealth;
        m_allTeams = GameMasterController.levelController.m_currentGameModeGameObj.GetComponent<GameMode>().m_teams;
        m_movementCooldown = 1.5f;
        m_fireCooldown = 5.0f;

    }
    void turnOrFire(direction dir)
    {
        if (m_direction == dir)
        {
            if (m_restTimeFromCoolDownFire <= 0.0f)
            {
                fire("ProjectileEnemy");
                m_restTimeFromCoolDownFire = m_fireCooldown;
                m_restTimeFromCoolDownMovement = m_movementCooldown;
            }
        }
        else
        {
            directionalInteract(dir);
            m_restTimeFromCoolDownMovement = m_movementCooldown;
        }
    }

    public override void Update()
    {
        if (GameMasterController.gameSate == GameState.Ingame)
        {
            if (m_restTimeFromCoolDownFire > 0.0f)
            {
                m_restTimeFromCoolDownFire -= Time.deltaTime;
            }
            if (m_restTimeFromCoolDownMovement > 0.0f)
            {
                m_restTimeFromCoolDownMovement -= Time.deltaTime;
            }

            foreach (KeyValuePair<string, Team> teams in m_allTeams)
            {
                //Needs rework if AI needs to be part of multiple teams
                if (teams.Value != m_partOfTeams[0])
                {
                    float minDistance = float.MaxValue;
                    foreach (KeyValuePair<string, PlayerEntity> player in teams.Value.m_players)
                    {
                        float currentDistance = Vector2.Distance(m_position, player.Value.m_position);
                        if (currentDistance < minDistance)
                        {
                            minDistance = currentDistance;
                            m_target = player.Value;
                        }
                    }
                }
            }
            //Check if it is on a shooting lane
            if (m_position.x == m_target.m_position.x)
            {
                float dirf = m_target.m_position.y - m_position.y;
                if (dirf > 0)
                {
                    turnOrFire(direction.up);
                }
                else
                {
                    turnOrFire(direction.down);
                }
            }
            else if (m_position.y == m_target.m_position.y)
            {
                float dirf = m_target.m_position.x - m_position.x;
                if (dirf > 0)
                {
                    turnOrFire(direction.right);
                }
                else
                {
                    turnOrFire(direction.left);
                }
            }
            else
            {
                float distanceX = m_target.m_position.x - m_position.x;
                direction xdir = direction.right;
                float distanceY = m_target.m_position.y - m_position.y;
                direction ydir = direction.up;
                if (distanceX < 0)
                {
                    distanceX *= -1;
                    xdir = direction.left;
                }
                if (distanceY < 0)
                {
                    distanceY *= -1;
                    ydir = direction.down;
                }
                if (distanceX < distanceY)
                {
                    if (xdir == direction.right)
                    {
                        if (m_currentLevel.m_assets.ContainsKey(m_position + new Vector2(1.0f, 0.0f)))
                        {
                            turnOrFire(xdir);
                        }
                        else if (m_restTimeFromCoolDownMovement <= 0.0f)
                        {
                            directionalInteract(xdir);
                            m_restTimeFromCoolDownMovement = m_movementCooldown;
                        }

                    }
                    else
                    {
                        if (m_currentLevel.m_assets.ContainsKey(m_position + new Vector2(-1.0f, 0.0f)))
                        {
                            turnOrFire(xdir);
                        }
                        else if (m_restTimeFromCoolDownMovement <= 0.0f)
                        {
                            directionalInteract(xdir);
                            m_restTimeFromCoolDownMovement = m_movementCooldown;
                        }

                    }
                }
                else
                {
                    if (ydir == direction.up)
                    {
                        if (m_currentLevel.m_assets.ContainsKey(m_position + new Vector2(0.0f, 1.0f)))
                        {
                            turnOrFire(ydir);
                        }
                        else if (m_restTimeFromCoolDownMovement <= 0.0f)
                        {
                            directionalInteract(ydir);
                            m_restTimeFromCoolDownMovement = m_movementCooldown;
                        }

                    }
                    else
                    {
                        if (m_currentLevel.m_assets.ContainsKey(m_position + new Vector2(0.0f, -1.0f)))
                        {
                            turnOrFire(ydir);
                        }
                        else if (m_restTimeFromCoolDownMovement <= 0.0f)
                        {
                            directionalInteract(ydir);
                            m_restTimeFromCoolDownMovement = m_movementCooldown;
                        }

                    }
                    //if (m_restTimeFromCoolDownMovement <= 0.0f)
                    //{
                    //    directionalInteract(ydir);
                    //    m_restTimeFromCoolDownMovement = m_movementCooldown;
                    //}
                }
                //Check shortest lane to move on
            }

        }

    }
}
