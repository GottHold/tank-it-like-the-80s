﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class defines the logic and everything the Game Mode Survive
/// </summary>
public class GM_Survive : GameMode {
    public UnityEvent m_levelEndReached;
    private bool initDone = false;
    private int m_currentGameModeLevel;

    private void init()
    {
        m_levelEndReached.AddListener(onLevelEnd);
        m_currentGameModeLevel = 1;
        m_statsController = new StatsController();
        initDone = true;

    }

    private void Update()
    {
        stateMachineHandler();
    }

    /// <summary>
    /// Prepares all game mode entities that the game mode can be played.
    /// </summary>
    public override void prepare()
    {
        if (!initDone) init();
        start();
        onLevelstart();


    }
 


    Vector2 spawnPlayerEntity(string prefab, string team)
    {
        return spawnPlayerEntity(prefab, team, "", new Vector2(float.NaN,float.NaN));
    }

    Vector2 spawnPlayerEntity(string prefab, string team, string playerIDoverride)
    {
        return spawnPlayerEntity(prefab, team, playerIDoverride, new Vector2(float.NaN, float.NaN));
    }

    /// <summary>
    /// Spawns PlayerEntity on the map
    /// </summary>
    /// <param name="prefab">the name of the prefab that should spawn</param>
    /// <param name="team">the team it should spawn in</param>
    /// <param name="playerIDoverride">the save playerid it should be saved as</param>
    /// <param name="overRidePosition">overrides the position something should spawn as</param>
    /// <returns>returns the 2D location of the spawned item</returns>
    Vector2 spawnPlayerEntity(string prefab, string team, string playerIDoverride, Vector2 overRidePosition)
    {
        Vector2 position;
        if (float.IsNaN(overRidePosition.x)) position = m_currentLevel.randomPositionThatIsUnused();
        else position = overRidePosition;
        string id = "";
        if (playerIDoverride.Length > 0) id = playerIDoverride;
        else id = ((SurvivalTeam)(m_teams[team])).m_nextIDGen.ToString();
        GameObject playerGameObj = Instantiate(m_levelController.m_playerEntitiesPrefab[prefab], m_currentLevel.gridVector2ToVector3(position), Quaternion.identity);
        PlayerEntity playerEntity = playerGameObj.GetComponent<PlayerEntity>();
        playerEntity.m_position = position;
        playerEntity.m_currentLevel = m_currentLevel;
        m_teams[team].m_players.Add(id, playerEntity);
        m_teams[team].m_players[id].m_partOfTeams = new List<Team>();
        m_teams[team].m_players[id].m_partOfTeams.Add(m_teams[team]);
        playerGameObj.transform.name = id;
        playerGameObj.transform.SetParent(m_teams[team].gameObject.transform);
        m_currentLevel.assignGameObjectToScene(playerGameObj, position);
        return position;
    }
    
    /// <summary>
    /// Creates all objects for this level
    /// </summary>
    void onLevelstart()
    {

        m_uiTools.setTextofTextComponentTo(GameMasterController.uiController.m_levelContentUI, m_currentGameModeLevel.ToString());
        m_teams = new Dictionary<string, Team>();
        GameObject teamPlayerGameObj = new GameObject("Team_Player");
        GameObject teamAIGameObj = new GameObject("Team_GameObj");
        teamPlayerGameObj.transform.SetParent(this.gameObject.transform);
        teamAIGameObj.transform.SetParent(this.gameObject.transform);
        m_teams.Add("Player", teamPlayerGameObj.AddComponent<SurvivalTeam>());
        m_teams.Add("AI", teamAIGameObj.AddComponent<SurvivalTeam>());
        m_teams["Player"].m_players = new Dictionary<string, PlayerEntity>();
        m_teams["AI"].m_players = new Dictionary<string, PlayerEntity>();
        m_currentLevel = m_levelController.m_currentlevelGameObj.GetComponent<Level>();
        spawnPlayerEntity("Player", "Player", "Player");
        spawnBase();
        for(int i=0; i<m_currentGameModeLevel; i++)
        {
            spawnPlayerEntity("AI", "AI");
        }

        m_teams["Player"].m_players["Player"].rotate(3, rotationDirection.clockWise);
        m_teams["Player"].m_players["Player"].m_playerDeath.AddListener(playerTeamDead);
        m_teams["Player"].m_players["Base"].m_playerDeath.AddListener(playerTeamDead);
        m_teams["AI"].connectEvents();
        m_teams["Player"].connectEvents();
        m_teams["AI"].m_teamExterminated.AddListener(aiDead);
        m_teams["Player"].m_teamHealthChanged.AddListener(refreshUI);
        m_teams["AI"].m_teamHealthChanged.AddListener(refreshUI);
        m_teams["Player"].m_players["Player"].m_health = 3.0f;
        m_teams["Player"].init();
        m_teams["AI"].init();
        refreshUI();
        
        
    }

    /// <summary>
    /// Refreshes UI once an important event is invoked
    /// </summary>
    void refreshUI()
    {
        m_uiTools.setTextofTextComponentTo(GameMasterController.uiController.m_baseContentUI, m_teams["Player"].m_players["Base"].m_health.ToString());
        m_uiTools.setTextofTextComponentTo(GameMasterController.uiController.m_playerContentUI, m_teams["Player"].m_players["Player"].m_health.ToString());
        m_uiTools.setTextofTextComponentTo(GameMasterController.uiController.m_enemiesContentUI, m_teams["AI"].m_playerAlive.ToString());
    }

    /// <summary>
    /// Spawn Logic around the main base spawn
    /// </summary>
    void spawnBase()
    {
        int borderSize = 1;
        Vector2 basePosition=spawnPlayerEntity("Base", "Player", "Base");
        int baseX = Convert.ToInt32(basePosition.x);
        int baseY = Convert.ToInt32(basePosition.y);
        m_teams["Player"].m_players["Base"].rotate(2, rotationDirection.clockWise);
        for(int y=baseY- borderSize; y<=baseY+ borderSize; ++y)
        {
            for(int x = baseX - borderSize; x <= baseX + borderSize; ++x)
            {
                if (x >= 0 && x < m_currentLevel.m_size&& y>=0 && y<m_currentLevel.m_size&&(!(x==baseX&&y==baseY)))
                {
                    Vector2 targetDestination = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
                    if(!m_currentLevel.m_assets.ContainsKey(targetDestination))
                    m_currentLevel.spawnAsset(new Vector2(x, y), "DestructableWallInv");
                }
            }
        }
    }

    void playerTeamDead()
    {
        Debug.Log("Player lost");
        m_score = 0;
        m_gameTime = 0;
        m_currentGameModeLevel = 0;
        m_levelEndReached.Invoke();
        end();
        m_statsController.resetPoints();
        start();
    }

    void aiDead()
    {
        Debug.Log("AI Died");
        m_statsController.addPoints(100);
        m_levelEndReached.Invoke();
    }

    /// <summary>
    /// Logic needs to destroy all entities so that the next level can be initialized.
    /// </summary>
    void onLevelEnd()
    {
        Debug.Log("Next Level");
        ++m_currentGameModeLevel;
  

        foreach (KeyValuePair<string, PlayerEntity> player in m_teams["AI"].m_players)
        {
            Destroy(player.Value.gameObject.transform.parent.gameObject);
            break;
        }
        foreach (KeyValuePair<string, PlayerEntity> player in m_teams["Player"].m_players)
        {
            Destroy(player.Value.gameObject.transform.parent.gameObject);
            break;
        }
        m_teams.Clear();
        int numberGen = m_levelController.m_freeMapNameInt;
        m_levelController.generateLevel(numberGen.ToString());
        Level old = m_currentLevel;
        m_levelController.m_currentLevel = numberGen.ToString();
        Destroy(old.gameObject);
        m_statsController.addPoints(1000);
        onLevelstart();
    }

}
