﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public string m_name;
    public float m_damage;
    public float m_speed;
    public Vector3 m_direction;

    private void Start()
    {
        m_name = "StdProjectile";
        m_damage = 1.0f;
        m_speed = 10.0f;
    }

    private void Update()
    {
        gameObject.transform.parent.position += m_direction * m_speed * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destructable destructable = collision.gameObject.GetComponentInChildren<Destructable>();
        PlayerEntity playerEntity = collision.gameObject.transform.parent.GetComponentInChildren<PlayerEntity>();
        if (!(destructable == null && playerEntity == null))
        {
            if (destructable == null)
            {
                playerEntity.m_health -= m_damage;
            }
            else
            {
                destructable.m_health -= m_damage;
            }
        }
        
        Destroy(gameObject.transform.parent.gameObject);
    }

    
}
